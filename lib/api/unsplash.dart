import 'dart:math';

import 'package:http/http.dart' as http;
import 'dart:convert';

class Unsplash {
  final String _accessKey =
      '6bc11faf4eaac5f9d5775a90c6e2048ba390d2c71aea887b3ea68e70c796e622';

  dynamic getRandomPhoto() async {
    final String _baseURL = 'https://api.unsplash.com/';
    var response;
    try {
      response = await http.get(
        '$_baseURL/photos/random?query=woodland+waterfall&orientation=portrait',
        headers: {
          'Authorization': 'Client-ID $_accessKey',
        },
      );
      var decodedJson = json.decode(response.body);
      var data = {
        'id': decodedJson['id'] + Random().toString(),
        'url': decodedJson['urls']['regular'],
        'description': decodedJson['description'],
        'width': decodedJson['width'],
        'height': decodedJson['height'],
      };
      return data;
    } catch (e) {
      print('error: $e');
    }
  }
}
