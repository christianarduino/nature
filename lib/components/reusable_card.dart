import 'package:flutter/material.dart';
import 'package:nature/api/unsplash.dart';
import 'package:nature/screens/details_page.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ReusableCard extends StatefulWidget {
  @override
  _ReusableCardState createState() => _ReusableCardState();
}

class _ReusableCardState extends State<ReusableCard> {
  var url;
  var id;
  var description;
  var heightpx;
  var widthpx;
  bool isLoaded = false;

  @override
  void initState() {
    super.initState();
    getImage();
  }

  void getImage() async {
    var image = await Unsplash().getRandomPhoto();
    setState(() {
      url = image['url'];
      id = image['id'];
      description = image['description'];
      heightpx = image['height'];
      widthpx = image['width'];
      isLoaded = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: width / 3,
          child: Text(
            description == null ? 'Description' : description,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: height / 52),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DetailsPage(
                  url: url,
                  hero: id,
                  heightpx: heightpx,
                  widthpx: widthpx,
                ),
              ),
            );
          },
          child: Container(
            color: Colors.transparent,
            padding: EdgeInsets.only(right: width / 15),
            height: height / 3,
            width: width / 2.5,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30.0),
              child: StreamBuilder(
                stream: null,
                builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                  if (isLoaded) {
                    return Hero(
                      tag: id,
                      child: Image(
                        image: CachedNetworkImageProvider(url),
                        fit: BoxFit.cover,
                      ),
                    );
                  } else {
                    return SpinKitWanderingCubes(
                      color: Colors.white,
                      size: 50.0,
                    );
                  }
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
