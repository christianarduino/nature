import 'package:flutter/material.dart';

class BottomContainer extends StatefulWidget {
  BottomContainer({this.height, this.url});

  double height;
  String url;

  @override
  _BottomContainerState createState() => _BottomContainerState();
}

class _BottomContainerState extends State<BottomContainer>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
    );
    animation = Tween<double>(
      begin: 0,
      end: widget.height / 9.5,
    ).animate(_controller)
      ..addListener(() => setState(() {}));
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: animation.value,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Color(0xFF001E26),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25.0),
          topRight: Radius.circular(25.0),
        ),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {},
          child: Container(
            child: Center(
              child: Text(
                'DOWNLOAD IT NOW',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: width / 23,
                  letterSpacing: 2,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
