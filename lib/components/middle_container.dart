import 'package:flutter/material.dart';

class MiddleContainer extends StatefulWidget {
  const MiddleContainer({
    Key key,
    this.height,
    this.heightpx,
    this.widthpx,
  }) : super(key: key);

  final double height;
  final int heightpx;
  final int widthpx;

  @override
  _MiddleContainerState createState() => _MiddleContainerState();
}

class _MiddleContainerState extends State<MiddleContainer>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 700),
    );

    animation = Tween<double>(
      begin: 0,
      end: widget.height / 2.3,
    ).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: animation.value,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(50.0),
          topRight: Radius.circular(50.0),
        ),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(
            bottom: widget.height / 9.5,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  top: widget.height * 0.05,
                  left: width / 20,
                ),
                child: Text(
                  'Parameter',
                  style: TextStyle(
                      color: Color(0xFF001E26),
                      fontWeight: FontWeight.bold,
                      fontSize: width / 25,
                      letterSpacing: 1),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: widget.heightpx * 0.002),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    PhotoInfo(
                        date: widget.heightpx.toString(), label: 'Height'),
                    PhotoInfo(date: widget.widthpx.toString(), label: 'Width'),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: width / 15,
                  vertical: widget.height / 30,
                ),
                child: Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dictum tristique diam sit amet vestibulum. Sed aliquam quam nec mauris vestibulum scelerisque. Aenean enim elit, bibendum ac bibendum id, dictum ac orci. Donec vitae metus et massa fermentum pulvinar. Etiam accumsan mi id laoreet porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque interdum consectetur risus eget vestibulum. In augue ipsum, pharetra imperdiet ipsum eget, ',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PhotoInfo extends StatelessWidget {
  PhotoInfo({this.date, this.label});

  final String date;
  final String label;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      elevation: 5,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(width / 20),
        child: Column(
          children: <Widget>[
            Text(
              date,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                letterSpacing: .9,
                fontSize: width / 22,
              ),
            ),
            SizedBox(
              height: height * 0.006,
            ),
            Text(
              label,
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
