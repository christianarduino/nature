import 'package:flutter/material.dart';
import 'package:nature/screens/home_page.dart';

void main() => runApp(Nature());

class Nature extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: HomePage(),
      ),
    );
  }
}
