import 'package:flutter/material.dart';
import 'package:nature/components/reusable_card.dart';
import 'package:nature/screens/prova.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

var image;

@override
  void initState() {
    super.initState();
    print('init state');
    image = AssetImage("images/background.jpg");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(image, context);
    print('didchangedependencies');
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: RaisedButton(
        onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => Prova()));},
        elevation: 7,
        color: Color(0xFF0dccdc),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: height / 40, horizontal: width / 20),
          child: Text(
            'Let\'s Go',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: width / 22,
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            color: Color.fromRGBO(20, 20, 20, 0.2),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: width / 22, top: height / 50),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Hi.',
                      style: TextStyle(
                        fontSize: width / 8,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      'Strive',
                      style: TextStyle(
                        fontSize: width / 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: height / 15,
                    ),
                    Material(
                      elevation: 20,
                      color: Colors.transparent,
                      child: Container(
                        width: width / 1.2,
                        child: Text(
                          'Here is the nature paradise \n welcome '
                          'your arrivalwe have \n prepared '
                          'a scene for you',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: width / 19,
                            color: Color(0xDDFFFFFF),
                            fontFamily: 'Source Sans Pro',
                            fontWeight: FontWeight.w400,
                            letterSpacing: 2,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: height / 10,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Padding(
                  padding: EdgeInsets.only(left: 8.0),
                  child: Row(
                    children: <Widget>[
                      ReusableCard(),
                      ReusableCard(),
                      ReusableCard(),
                      ReusableCard(),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
