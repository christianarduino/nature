import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nature/components/bottom_container.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:nature/components/middle_container.dart';

class DetailsPage extends StatefulWidget {
  DetailsPage({this.url, this.hero, this.widthpx, this.heightpx});

  String url;
  String hero;
  int heightpx;
  int widthpx;

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage>
    with SingleTickerProviderStateMixin {
  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Hero(
              tag: widget.hero,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(widget.url),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Container(
              color: Color(0x33000000),
            ),
            Padding(
              padding: EdgeInsets.all(width / 27),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                    ),
                    iconSize: width / 13,
                    onPressed: () => Navigator.pop(context),
                  ),
                  Container(
                    child: IconButton(
                      icon: Icon(
                        isFavorite ? Icons.favorite : Icons.favorite_border,
                        color: Colors.white,
                      ),
                      iconSize: width / 13,
                      onPressed: () => setState(() => isFavorite = !isFavorite),
                    ),
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: MiddleContainer(
                height: height,
                heightpx: widget.heightpx,
                widthpx: widget.widthpx,
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: BottomContainer(
                height: height,
                url: widget.url,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
